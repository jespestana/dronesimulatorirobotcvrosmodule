//////////////////////////////////////////////////////
//  droneSimulatorGridROSModule_Node.h
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"



//cv module
#include "multicameraRobotTrackerSimulator.h"


//Communication Definition
#include "communication_definition.h"

//Nodes names
//MODULE_NAME_DRONE_KEYPOINTS_GRID_DETECTOR
#include "nodes_definition.h"





using namespace std;

int main(int argc, char **argv)
{
    //Init
    ros::init(argc, argv, MODULE_NAME_DRONE_SIMULATOR_GRID_DETECTOR); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!

    std::string node_name=ros::this_node::getName();

    cout<<"Starting "<<node_name<<endl;


    //Config file
    std::string sensor_configuration;
    ros::param::get("~sensor_configuration",sensor_configuration);
    cout<<"sensor_configuration="<<sensor_configuration<<endl;
    if (sensor_configuration.length()==0)
    {
        cout<<"[ROSNODE] Error with sensor_configuration";
    }

    //DronePose subscription topic
    std::string drone_pose_topic_name;
    ros::param::get("~drone_pose_topic_name",drone_pose_topic_name);
    cout<<"drone_pose_topic_name="<<drone_pose_topic_name<<endl;
    if (drone_pose_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with drone_pose_topic_name";
    }

    //Keypoints publ topic
    std::string detected_irobots_topic_name;
    ros::param::get("~detected_irobots_topic_name",detected_irobots_topic_name);
    cout<<"detected_irobots_topic_name="<<detected_irobots_topic_name<<endl;
    if (detected_irobots_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with detected_irobots_topic_name";
    }



    //Class
    MulticameraRobotTrackeriRobotCVROSModule myMulticameraRobotTrackeriRobotCVROSModule;


    if(!myMulticameraRobotTrackeriRobotCVROSModule.setSensorConfig(sensor_configuration))
        return 0;

    if(!myMulticameraRobotTrackeriRobotCVROSModule.setTopicConfigs(drone_pose_topic_name,detected_irobots_topic_name))
        return 0;


    myMulticameraRobotTrackeriRobotCVROSModule.open(n);


    //Loop -> shyncronous Module
    while(ros::ok())
    {
        ros::spinOnce();

        //Run retina
        if(!myMulticameraRobotTrackeriRobotCVROSModule.run())
        {
            //cout<<"error"<<endl;
        }

        //Sleep
        myMulticameraRobotTrackeriRobotCVROSModule.sleep();
    }

    return 1;
}

