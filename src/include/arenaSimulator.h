//////////////////////////////////////////////////////
//  arenaSimulator.h
//
//  Created on: Mar 20, 2013
//      Author: jl.sanchez
//
//  Last modification on: Mar 20, 2013
//      Author: jl.sanchez
//
//////////////////////////////////////////////////////



#ifndef ARENA_SIMULATOR_H
#define ARENA_SIMULATOR_H


#include <iostream>
#include <string>
#include <vector>


#include "opencv2/opencv.hpp"



#include "geometry.h"





/////////////////////////////////////////
// Class World
//
//   Description
//
/////////////////////////////////////////
class StaticWorld
{
    //Reference System
protected:
    std::vector<cv::Point3d> referenceSystem3D_Origin; //O
    std::vector<cv::Point3d> referenceSystem3D; //xu, yu, zu

    //Points
protected:
    std::vector<cv::Point3d> points3D; //{x, y, z}

    //Lines
protected:
    std::vector< Line<cv::Point3d> > lines3D;



public:
    StaticWorld();
    ~StaticWorld();


public:
    int getReferenceSystem3D(std::vector<cv::Point3d>& referenceSystem3D_Origin, std::vector<cv::Point3d>& referenceSystem3D);

    //Points
public:
    int setPoints3D(const std::vector<cv::Point3d>& points3D);
    int getPoints3D(std::vector<cv::Point3d>& points3D);

    //Lines
public:
    int setLines3D(const std::vector< Line<cv::Point3d> >& lines3D);
    int getLines3D(std::vector< Line<cv::Point3d> >& lines3D);


};





/////////////////////////////////////////
// Class Arena
//
//   Description
//
/////////////////////////////////////////
class Arena : public StaticWorld
{


public:
    Arena();
    ~Arena();



};







#endif
